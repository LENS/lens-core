# Building LENS

To build LENS, first ensure you have these prerequisites:

* A Unix-like environment with the [GNU Bash](https://www.gnu.org/software/bash/) shell. It is very strongly recommended to use a Linux distribution, because otherwise you will not be able to run the LENS binaries you produce. BSD, macOS, and Windows may be usable for builds, but are not suitable development environments unless paired with a Linux kernel compatibility layer or a Linux virtual machine. Windows has not been tested, but it is recommended to use the Windows Subsystem for Linux; environments like Cygwin may be able to produce binaries but will not be able to run them.
* A nightly version of Rust. You can install a nightly using [Rustup](https://rustup.rs/), which you may be able to get from your package manager or as a fallback by running a script provided by the Rustup project.

To build, change directories to the `lens-core` repository and run the `build` script (you do not need to use Bash to do this, but the script requires Bash to run):

```
$ cd lens-core
$ ./build
```

If successful, the build process creates a `staging` directory holding the `system.lens-os.xyz` compartment. It also archives this into a `tar` archive package.

LENS currently builds only for `x86_64`, and its binaries will only run on Linux (not other Unix-like environments). The former is a limitation that can be overcome, but the latter is a design decision.

You can customize various aspects of the build by setting environment variables; these are explained at the top of the build script.
