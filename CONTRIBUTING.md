# Contributing to LENS

There are several ways to contribute to LENS. Perhaps the easiest, and in some ways the most important, is critique and suggestions on LENS' design. While your criticism or suggestion will not be automatically accepted, it will be appreciated and seriously considered. Also appreciated are code contributions, whether to fix bugs, implement features, improve general quality, or otherwise. The project is not currently soliciting other kinds of contribution, but feel free to offer if you so wish.

***IMPORTANT NOTE***: The Gitea software used by `git.lens-os.xyz` supports GitHub-style pull requests and issues; however, these require creating an account, so LENS does not use them. The contents of these systems are for archival following a migration from Git hosting elsewhere.

Again: ***Do not try to use pull requests or issues*** on `git.lens-os.xyz`!

**TODO**: Setting up dedicated mailing lists for LENS will simplify contribution somewhat and is a priority for the near future.

## Comments, criticism, and suggestions

There are several ways to send these in, and different ones will likely be preferred by different people.

### Email

The current preferred method is to send mail to `public00178@alm.website` (this address may be replaced if it receives spam). It will probably be read within a day or so.

### ActivityPub social networking

If you use ActivityPub social networking software (aka the "Fediverse"), such as Mastodon, Pleroma, Misskey, or many others, you may also send a note (whether mention or direct message) to `@alexandra@nixnet.social`.

## Code

There are also several ways to submit code contributions. In any case, please make sure to include some description of the changes you made and why, at least at a high level.

If you make code changes, please make sure to [build LENS](BUILDING.md) with them and test the modified components. If you need or want help at any point in the contribution process, get in touch by email or ActivityPub as described above.

Also, consider pulling the latest trunk and rebasing on top of it occasionally as you work, to reduce the chance of merge conflicts. If merge conflicts do happen when bringing in your changes, they will be resolved by the merging maintainer if the correct resolution seems clear.

### Git pull requests

After cloning the appropriate repository, branching off `trunk`, making your changes, and pushing them to another publically-accessible Git server (such as a personal Git server, [Codeberg](https://codeberg.org), or [GitLab.com](https://gitlab.com)), you can use [`git request-pull`](https://www.git-scm.com/docs/git-request-pull) to generate a form-letter request to pull your changes from there. The output text it generates can be copy-pasted or redirected directly into an email body or email attachment and mailed to the same email address as given above for commentary and suggestions.

A basic example of using `git request-pull`:

```
$ git branch my-change
$ # ... changes are made ...
$ git add -A
$ git commit -m "Make my change"
$ git push git@git.example.net:lens-core my-change
$ git request-pull origin/trunk https://git.example.net/lens-core my-change | mail public00178@alm.website
```

While this option is convenient when you have a public Git server, setting one up if you don't already have one is not easy. The last option should be workable for anyone, though it can be more complicated.

### Mailed patches

After cloning the appropriate repository, branching off `trunk`, and making your changes, you can use [`git format-patch`](https://git-scm.com/docs/git-format-patch) to create patch files containing your changes for sending via email. The files that `git format-patch` generates can be sent directly (using [`git send-email`](https://git-scm.com/docs/git-send-email) or by copying them into a compatible mail client's local Drafts mailbox) or as attachments. Mail your patches to the same email address given above for commentary and suggestions.

A basic example of using `git format-patch`, assuming an email account has already been configured for `git send-email`:

```
$ git branch my-change
$ # ... changes are made ...
$ git add -A
$ git commit -m "Make my change"
$ git format-patch origin/trunk -o patches -MC --cover-letter
$ git send-email patches --to=public00178@alm.website
```
