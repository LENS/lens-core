# LENS

Rust workspace containing the core LENS libraries as well as (for now) a small number of demonstration programs for them.

Design planning is done in the `design` repository.

***This project does not work like a GitHub repository!*** See [the `CONTRIBUTING` file](CONTRIBUTING.md) for details on how to participate, if you're so inclined.
