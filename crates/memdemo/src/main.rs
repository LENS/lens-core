//! Creates 100,000 mid-sized objects (64-byte arrays), verifies they are intact, then drops them, to test the LENS memory allocator.
#![no_std]
#![no_main]

extern crate alloc;
extern crate lens_system;

const COUNT: usize = 100000;
const SIZE: usize = 64;

#[no_mangle]
fn main() {
    let mut vec = alloc::vec::Vec::<alloc::boxed::Box<[u8; SIZE]>>::with_capacity(COUNT);
    for i in 0..COUNT {
        vec.push(alloc::boxed::Box::<[u8; SIZE]>::new([(i % 256) as u8; SIZE]));
    }
    for i in 0..COUNT {
        let arr = &vec[i];
        assert_eq!(arr[0], (i % 256) as u8);
        assert_eq!(arr[SIZE - 1], (i % 256) as u8);
    }
}
