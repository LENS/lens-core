// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![no_std]
#![no_main]
extern crate alloc;
extern crate lens_flows;
extern crate lens_messenger;
extern crate lens_postcardflows;
extern crate serde;

use alloc::boxed::Box;
use alloc::string::{String, ToString};
use lens_flows::object::Send;
use lens_postcardflows::PostcardSendFilter;
use serde::Serialize;

#[derive(Serialize)]
struct Widget {
    manufacturer: String,
    model: String,
}

#[no_mangle]
fn main() {
    let ep = Box::new(
        lens_messenger::Endpoint::connect("/tmp/lens-msgserver")
            .expect("Failed to connect endpoint"),
    );
    let flow = PostcardSendFilter::new(ep);
    let widget = Widget {
        manufacturer: "ACME".to_string(),
        model: "Hyper Widgetizer".to_string(),
    };
    flow.send(widget);
}
