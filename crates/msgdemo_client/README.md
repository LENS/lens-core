# `msgdemo_client`

The client side of a small demonstration of LENS messengers (`lens_messenger`) and object flows (`lens_flows` and `lens_postcardflows`). The `msgdemo_client` program connects to the server's messenger and sends a `Widget` structure.
