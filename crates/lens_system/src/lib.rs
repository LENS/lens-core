// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![cfg_attr(feature = "no_std", no_std)]

#[cfg(feature = "lens_runtime")]
extern crate lens_runtime;
#[cfg(feature = "lens_memory")]
extern crate lens_memory;

extern crate alloc;
extern crate lens_syscalls;

use core::fmt::{Debug, Display};

#[cfg(feature = "lens_memory")]
#[global_allocator]
static ALLOC: lens_memory::DefaultAllocator = lens_memory::DefaultAllocator::new();

/// Contains functionality for working with handles to kernel objects.
pub mod handle;

/// An imitation of the non-deprecated parts of `std::error::Error` because it's missing from `core`.
pub trait Error: Debug + Display {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

pub type Result<T> = core::result::Result<T, alloc::boxed::Box<dyn Error>>;

/// An error received from the kernel. Corresponds to an errno(3) value.
#[derive(Debug)]
pub struct KernelError {
    pub errno: i32,
}

impl Error for KernelError {
    fn source(&self) -> Option<&(dyn Error + 'static)> {
        None
    }
}

impl core::fmt::Display for KernelError {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "kernel error number {}", self.errno)
    }
}
