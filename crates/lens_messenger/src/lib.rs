// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![no_std]

extern crate alloc;
extern crate lens_flows;
extern crate lens_syscalls;
extern crate lens_system;

use alloc::boxed::Box;
use lens_flows::packet::{Receive, Send};
use lens_system::handle::Handle;
use lens_system::{KernelError, Result};

use alloc::vec::Vec;
use core::ffi::c_void;
use core::mem::size_of;
use core::ptr::null_mut;

/// A messenger server, which accepts connections from clients.
pub struct Server {
    handle: Handle,
}

/// A connected messenger endpoint.
pub struct Endpoint {
    handle: Handle,
}

const BUFFER_SIZE: i32 = 8;

impl Server {
    /// Create a Server at the given path.
    pub fn new(path: &str) -> Result<Server> {
        let fd: i32;
        let mut path_vec = path.as_bytes().to_vec();
        path_vec.resize(108, 0);
        let path_slice = &path_vec[..];

        fd = lens_syscalls::socket(1, 5, 0); // AF_UNIX, SOCK_SEQPACKET

        if fd < 0 {
            return Err(Box::new(KernelError { errno: -fd }));
        }

        let sv = Server {
            handle: Handle { descriptor: fd },
        };
        let mut addr: lens_syscalls::sockaddr_un = lens_syscalls::sockaddr_un {
            sun_family: 1,
            sun_path: [0; 108],
        };
        addr.sun_path.copy_from_slice(path_slice);

        let bind_error = lens_syscalls::bind(
            sv.handle.descriptor,
            &mut addr as *mut lens_syscalls::sockaddr_un as *mut lens_syscalls::sockaddr,
            size_of::<lens_syscalls::sockaddr_un>(),
        );
        if bind_error < 0 {
            return Err(Box::new(KernelError { errno: -bind_error }));
        }

        let listen_error = lens_syscalls::listen(sv.handle.descriptor, BUFFER_SIZE);
        if listen_error < 0 {
            return Err(Box::new(KernelError {
                errno: -listen_error,
            }));
        }

        Ok(sv)
    }

    /// Wait for a new connection to the server and create an Endpoint for it.
    pub fn accept(&self) -> Result<Endpoint> {
        let fd: i32;

        fd = lens_syscalls::accept(self.handle.descriptor, null_mut(), 0);

        if fd < 0 {
            return Err(Box::new(KernelError { errno: -fd }));
        }

        Ok(Endpoint {
            handle: Handle { descriptor: fd },
        })
    }
}

impl Endpoint {
    /// Connect an Endpoint to the given path.
    pub fn connect(path: &str) -> Result<Endpoint> {
        let fd: i32;
        let mut path_vec = path.as_bytes().to_vec();
        path_vec.resize(108, 0);
        let path_slice = &path_vec[..];
        let err: i32;

        fd = lens_syscalls::socket(1, 5, 0); // AF_UNIX, SOCK_SEQPACKET

        if fd < 0 {
            return Err(Box::new(KernelError { errno: -fd }));
        }

        let ep = Endpoint {
            handle: Handle { descriptor: fd },
        };
        let mut addr: lens_syscalls::sockaddr_un = lens_syscalls::sockaddr_un {
            sun_family: 1,
            sun_path: [0; 108],
        };
        addr.sun_path.copy_from_slice(path_slice);

        err = lens_syscalls::connect(
            ep.handle.descriptor,
            &mut addr as *mut lens_syscalls::sockaddr_un as *mut lens_syscalls::sockaddr,
            size_of::<lens_syscalls::sockaddr_un>(),
        );

        if err < 0 {
            return Err(Box::new(KernelError { errno: -err }));
        }

        Ok(ep)
    }

    pub fn descriptor_is_endpoint(descriptor: i32) -> Result<bool> {
        let mut stat: lens_syscalls::statx_data = lens_syscalls::statx_data {
            // Initialize empty data.
            stx_mask: 0,
            stx_blksize: 0,
            stx_attributes: 0,
            stx_nlink: 0,
            stx_uid: 0,
            stx_gid: 0,
            stx_mode: 0,
            __spare1: 0,
            stx_ino: 0,
            stx_size: 0,
            stx_blocks: 0,
            stx_attributes_mask: 0,
            stx_atime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_btime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_ctime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_mtime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_rdev_major: 0,
            stx_rdev_minor: 0,
            stx_dev_major: 0,
            stx_dev_minor: 0,
            __spare2: [0; 14],
        };
        let empty: [u8; 1] = [0; 1];

        let err = lens_syscalls::statx(
            descriptor,
            &empty[0] as *const u8,
            0x1000,
            1,
            &mut stat as *mut lens_syscalls::statx_data,
        ); // Ask kernel for descriptor's type.

        if err < 0 {
            return Err(Box::new(KernelError { errno: -err }));
        }

        if stat.stx_mode & 0xc000 == 0xc000 {
            // Descriptor is socket.
            let mut outbuf: i32 = 0;
            let mut bufsz: usize = 4;

            let err = lens_syscalls::getsockopt(
                descriptor,
                1,
                27,
                &mut outbuf as *mut i32 as *mut c_void,
                &mut bufsz as *mut usize as *mut c_void,
            ); // SO_DOMAIN

            if err < 0 {
                return Err(Box::new(KernelError { errno: -err }));
            }

            if outbuf == 1 {
                // Socket is AF_LOCAL.
                let err = lens_syscalls::getsockopt(
                    descriptor,
                    1,
                    3,
                    &mut outbuf as *mut i32 as *mut c_void,
                    &mut bufsz as *mut usize as *mut c_void,
                ); // SO_TYPE

                if err < 0 {
                    return Err(Box::new(KernelError { errno: -err }));
                }

                if outbuf == 5 {
                    // Socket is SOCK_SEQPACKET; this is a messenger.
                    return Ok(true);
                }
            }
        }
        Ok(false)
    }

    pub fn from_descriptor(descriptor: i32) -> Result<Endpoint> {
        // TODO: Check that the descriptor refrences an endpoint and not a server, file etc.
        Ok(Endpoint {
            handle: Handle {
                descriptor: descriptor,
            },
        })
    }
}

impl Receive for Endpoint {
    /// Receive a packet (up to max_size bytes) from the connected endpoint. Waits for a message to arrive if none in the queue.
    fn receive(&self, max_size: usize) -> Result<Vec<u8>> {
        let mut buf: Vec<u8> = Vec::<u8>::with_capacity(max_size);
        buf.resize(max_size, 0);
        let buf_slice: &mut [u8] = &mut buf[..];

        let mut iovec = lens_syscalls::iovec {
            iov_base: buf_slice.as_mut_ptr() as *mut c_void,
            iov_len: max_size,
        };
        let mut header = lens_syscalls::msghdr {
            msg_name: null_mut(),
            msg_namelen: 0,
            msg_iov: &mut iovec as *mut lens_syscalls::iovec,
            msg_iovlen: 1,
            msg_control: null_mut(),
            msg_controllen: 0,
            msg_flags: 0,
        };

        let received: isize;
        received = lens_syscalls::recvmsg(
            self.handle.descriptor,
            &mut header as *mut lens_syscalls::msghdr,
            0,
        );

        if received < 0 {
            return Err(Box::new(KernelError {
                errno: -(received as i32),
            }));
        }

        buf.truncate(received as usize);
        buf.shrink_to_fit();

        Ok(buf)
    }
}

impl Send for Endpoint {
    /// Send a packet to the connected endpoint. Waits for the queue to drain if full.
    fn send(&self, packet: &mut [u8]) -> Result<()> {
        let send_error: i32;
        let mut iovec = lens_syscalls::iovec {
            iov_base: packet.as_mut_ptr() as *mut c_void,
            iov_len: packet.len(),
        };
        let mut header = lens_syscalls::msghdr {
            msg_name: null_mut(),
            msg_namelen: 0,
            msg_iov: &mut iovec as *mut lens_syscalls::iovec,
            msg_iovlen: 1,
            msg_control: null_mut(),
            msg_controllen: 0,
            msg_flags: 0,
        };

        send_error = lens_syscalls::sendmsg(
            self.handle.descriptor,
            &mut header as *mut lens_syscalls::msghdr,
            128,
        ); // 128 = MSG_EOR

        if send_error != 0 {
            return Err(Box::new(KernelError { errno: -send_error }));
        }

        Ok(())
    }
}
