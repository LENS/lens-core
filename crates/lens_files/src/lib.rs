// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

#![no_std]
extern crate alloc;
extern crate lens_flows;
extern crate lens_syscalls;
extern crate lens_system;

use alloc::boxed::Box;
use alloc::vec::Vec;
use lens_flows::byte::{Read, Write};
use lens_system::handle::Handle;
use lens_system::{KernelError, Result};

/// A disk file.
pub struct File {
    handle: Handle,
}

impl Read for File {
    fn read(&self, size: usize) -> Result<Vec<u8>> {
        let mut buf: Vec<u8> = Vec::<u8>::with_capacity(size);
        buf.resize(size, 0);
        let buf_slice: &mut [u8] = &mut buf[..];
        let result: usize;

        result = lens_syscalls::read(self.handle.descriptor, buf_slice.as_mut_ptr(), size);

        buf.truncate(result);
        buf.shrink_to_fit();

        if (result as i32) < 0 {
            return Err(Box::new(KernelError {
                errno: (result as i32 * -1),
            }));
        }
        return Ok(buf);
    }
}

impl Write for File {
    fn write(&self, data: &[u8]) -> Result<usize> {
        let count = data.len();
        let result: usize;

        result = lens_syscalls::write(self.handle.descriptor, &data[0] as *const u8, count);

        if (result as i32) < 0 {
            return Err(Box::new(KernelError {
                errno: (result as i32 * -1),
            }));
        }
        return Ok(result as usize);
    }
}

// TODO: Seekable.

impl File {
    /// Open the file at the given path. If `write` is true, open for writing and create the file if non-existant. If creating, use mode `mode`.
    pub fn open(name: &str, write: bool, mode: u16) -> Result<File> {
        let flag = if write {
            0x40 | 0x02 // O_CREAT | O_RDWR
        } else {
            0x00 // O_RDONLY
        };
        let result: i32;

        result = lens_syscalls::open(&name.as_bytes()[0] as *const u8, flag, mode);

        if result < 0 {
            return Err(Box::new(KernelError {
                errno: (result * -1),
            }));
        }

        return Ok(File {
            handle: Handle { descriptor: result },
        });
    }

    // Determine if the specified file descriptor refers to a regular file.
    pub fn descriptor_is_file(descriptor: i32) -> Result<bool> {
        let mut stat: lens_syscalls::statx_data = lens_syscalls::statx_data {
            // Initialize empty data.
            stx_mask: 0,
            stx_blksize: 0,
            stx_attributes: 0,
            stx_nlink: 0,
            stx_uid: 0,
            stx_gid: 0,
            stx_mode: 0,
            stx_ino: 0,
            __spare1: 0,
            stx_size: 0,
            stx_blocks: 0,
            stx_attributes_mask: 0,
            stx_atime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_btime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_ctime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_mtime: lens_syscalls::statx_timestamp {
                tv_sec: 0,
                tv_nsec: 0,
            },
            stx_rdev_major: 0,
            stx_rdev_minor: 0,
            stx_dev_major: 0,
            stx_dev_minor: 0,
            __spare2: [0; 14],
        };
        let empty: [u8; 1] = [0; 1];

        let err = lens_syscalls::statx(
            descriptor,
            &empty[0] as *const u8,
            0x1000,
            1,
            &mut stat as *mut lens_syscalls::statx_data,
        ); // Ask kernel for descriptor's type.

        if err < 0 {
            return Err(Box::new(KernelError { errno: -err }));
        }

        if stat.stx_mode & 0x8000 == 0x8000 {
            // S_IFREG is set; this is a regular file.
            return Ok(true);
        }
        Ok(false)
    }

    /// Create a `File` from a given raw file descriptor. The `File` takes ownership of the descriptor and will close it when dropped. Avoid using on things on which `read`/`write`/`lseek` don't work.
    pub fn from_descriptor(descriptor: i32) -> Result<File> {
        // TODO: Check that the descriptor references a file and not, say, an inotify watcher.
        return Ok(File {
            handle: Handle { descriptor },
        });
    }
}

// Cargo tests don't like no_std.
/*#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn fileWrite() {
    let f = File::open("/tmp/filedata\0", true, 0x01A4).expect("Failed to open file");
    f.write(b"This is a test.\n").expect("Failed to write file");
  }

  #[test]
  fn stdWrite() {
    let f = File::from_descriptor(1).expect("Failed to make File from stdout");
    f.write(b"This is a test.\n").expect("Failed to write stdout");
  }
}*/
