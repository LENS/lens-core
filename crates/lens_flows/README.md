# `lens_flows`

The LENS flows generic I/O framework. Written for the LENS project.

Flows are simplex or full-duplex communications channels carrying sequences of bytes (byte flows), discrete packets (packet flows), or Rust objects (object flows).

This crate contains the traits that define flows, as well as `Null` flows that do nothing.
