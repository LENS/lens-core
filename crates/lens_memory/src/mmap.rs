use core::alloc::{GlobalAlloc, Layout};

pub struct MmapAllocator;

impl MmapAllocator {
    pub const fn new() -> MmapAllocator {
        MmapAllocator {}
    }
}

unsafe impl GlobalAlloc for MmapAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        return lens_syscalls::mmap(0, layout.size(), 0x01 | 0x02, 0x20 | 0x02, -1, 0);
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        lens_syscalls::munmap(ptr, layout.size());
    }
}
