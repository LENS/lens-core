#![no_std]
#![feature(alloc_error_handler)]

extern crate bitmaps;
extern crate lens_syscalls;
extern crate typenum;

use core::alloc::Layout;

mod mmap;

mod blockmap;

#[cfg(feature = "blockmap")]
pub type DefaultAllocator = blockmap::BlockmapAllocator;

#[cfg(feature = "mmap")]
pub type DefaultAllocator = mmap::MmapAllocator;

#[alloc_error_handler]
fn alloc_error(_layout: Layout) -> ! {
    panic!("Allocation error.");
}

// Can't do cargo tests without libstd.
/*#[cfg(test)]
mod tests {
  #[test]
  fn vec() {
    let mut v = vec!(1, 2, 3); // Allocate.
    assert_eq!(v[0], 1);
    assert_eq!(v[1], 2);
    assert_eq!(v[2], 3);

    v = vec!(4, 5, 6); // Release/reallocate.
    assert_eq!(v[0], 4);
    assert_eq!(v[1], 5);
    assert_eq!(v[2], 6);
  }
}*/
