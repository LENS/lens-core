/// Block map allocator. Not thread-safe, because LENS is currently single-threaded. Divides pages into blocks of equal size and keeps a bitmap of which blocks are allocated or free in the first block of each page. Pages are kept on a linked list. Anything too big to fit in one page is `mmap`ed. Not an efficient allocator, but better than `mmap`ing every allocation.
use core::alloc::{GlobalAlloc, Layout};

pub enum BlockmapError {
    /// There was not enough available memory to satisfy the allocation.
    OutOfMemory,
}

#[cfg(target_arch = "x86_64")]
const PAGESIZE: usize = 4096;

// Defined for each architecture as the first power of two where (BLOCKSIZE - POINTERSIZE) * 8 * BLOCKSIZE >= PAGESIZE.
#[cfg(target_arch = "x86_64")]
const BLOCKSIZE: usize = 32;

// Must be BLOCKSIZE or smaller.
#[cfg(target_arch = "x86_64")]
pub struct PageHeader {
    next: *mut PageHeader,                       // Zero in last page.
    map: bitmaps::Bitmap<typenum::consts::U128>, // 8 * 32 = 4096
}

pub struct PageList {
    first_page: *mut PageHeader, // Both of these may be 0 if no pages yet allocated.
}

pub struct BlockmapAllocator {
    list: core::cell::RefCell<PageList>, // If we go multi-threaded, this has to be replaced with a Mutex or similar.
}

unsafe impl Sync for BlockmapAllocator {} // BLATANT LIE

impl BlockmapAllocator {
    /// Find a continguous run of `n` blocks, mark them allocated, and return a pointer to the first byte. Returns `BlockmapError::OutOfMemory` if no page has a large enough continguous free run.
    unsafe fn find_blocks(&self, n: usize) -> Result<*mut u8, BlockmapError> {
        let mut prev_page: *mut PageHeader = 0 as *mut PageHeader;
        let mut current: *mut PageHeader = self.list.borrow_mut().first_page;

        // Walk page list.
        while current != 0 as *mut PageHeader {
            let mut contiguous: usize = 0; // Number of contiguous blocks encountered in this run.
            let mut first: usize = 0; // First block index in this chain.
            let mut prev: usize = usize::MAX - 1; // Most recent block index in this chain. Max - 1 as default to guarantee that, even with underflow from 0, the initial previous always looks discontiguous from the current index.

            // Skip pages that definitely don't have enough blocks.
            if (*current).map.len() >= n {
                // Look for n 1 bits (free blocks) in a row. Bitmap iterator yields indexes of 1 bits (free blocks).
                for index in (*current).map.into_iter() {
                    // Reset if the current index is not one more than the previous.
                    if prev != (index - 1) {
                        contiguous = 0;
                        first = index;
                        // prev is set below.
                    }

                    contiguous += 1;
                    prev = index;

                    // Found enough blocks.
                    if contiguous >= n {
                        // Mark blocks as used.
                        for i in first..=index {
                            (*current).map.set(i, false);
                        }

                        // Remove fully-used pages from the free list.
                        if (*current).map.is_empty() {
                            if prev_page as usize == 0 {
                                self.list.borrow_mut().first_page = (*current).next;
                            } else {
                                (*prev_page).next = (*current).next;
                            }
                        }

                        // Calculate pointer to first byte of blocks.
                        return Ok(((current as usize) + first * BLOCKSIZE) as *mut u8);
                    }
                }
            }

            current = (*current).next;
        }
        Err(BlockmapError::OutOfMemory) // After checking all pages, didn't find enough blocks.
    }

    /// Mark `n` blocks beginning at `addr` as free.
    unsafe fn free_blocks(&self, addr: *mut u8, n: usize) {
        let page = ((addr as usize / PAGESIZE) * PAGESIZE) as *mut PageHeader; // Find the page this address is in by rounding down to a multiple of PAGESIZE.
        let must_relist = (*page).map.is_empty(); // If the map is all 0s, the page has been taken off the freelist and needs to be readded.
        let first = (addr as usize - page as usize) / BLOCKSIZE; // Find the block number by dividing the offset by BLOCKSIZE.

        // Mark blocks as free.
        for i in first..first + n {
            (*page).map.set(i, true);
        }

        // Readd to freelist.
        if must_relist {
            let mut list = self.list.borrow_mut();
            (*page).next = list.first_page;
            list.first_page = page;
        }
    }

    /// Allocate a new page from the kernel, initialize the header, and add it to the page list. Returns `BlockmapError::OutOfMemory` if the system call to allocate memory fails.
    unsafe fn add_page(&self) -> Result<*mut PageHeader, BlockmapError> {
        let result = lens_syscalls::mmap(0, PAGESIZE, 0x01 | 0x02, 0x20 | 0x02, -1, 0);
        if (result as isize) < 0 {
            return Err(BlockmapError::OutOfMemory);
        }

        let mut list = self.list.borrow_mut();
        let page: *mut PageHeader = result as *mut PageHeader;
        (*page).next = list.first_page;
        (*page).map = bitmaps::Bitmap::new(); // Initialize map with all blocks free except the first.
        (*page).map.invert();
        (*page).map.set(0, false);

        list.first_page = page;
        Ok(page)
    }

    /// Allocate `n` blocks, allocating a new page if necessary.
    unsafe fn alloc_blocks(&self, n: usize) -> Result<*mut u8, BlockmapError> {
        match self.find_blocks(n) {
            Ok(p) => Ok(p),
            Err(_) => {
                let page = self.add_page()?;

                // Mark n blocks at the start of the page used.
                for i in 1..=n {
                    (*page).map.set(i, false);
                }
                Ok((page as usize + BLOCKSIZE) as *mut u8)
            }
        }
    }

    pub const fn new() -> BlockmapAllocator {
        BlockmapAllocator {
            list: core::cell::RefCell::<PageList>::new(PageList {
                first_page: 0 as *mut PageHeader,
            }),
        }
    }
}

unsafe impl GlobalAlloc for BlockmapAllocator {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        if layout.size() > PAGESIZE - BLOCKSIZE {
            let result = lens_syscalls::mmap(0, layout.size(), 0x01 | 0x02, 0x20 | 0x02, -1, 0);
            if (result as isize) < 0 {
                return 0 as *mut u8;
            }
            return result;
        } else {
            let blocks = (layout.size() + BLOCKSIZE - 1) / BLOCKSIZE; // Increment to almost the next multiple before dividing to imitate ceil() for positive integers.
            match self.alloc_blocks(blocks) {
                Ok(p) => p,
                Err(_) => 0 as *mut u8,
            }
        }
    }

    unsafe fn dealloc(&self, ptr: *mut u8, layout: Layout) {
        if layout.size() > PAGESIZE - BLOCKSIZE {
            lens_syscalls::munmap(ptr, layout.size());
        } else {
            let blocks = (layout.size() + BLOCKSIZE - 1) / BLOCKSIZE; // Increment to almost the next multiple before dividing to imitate ceil() for positive integers.
            self.free_blocks(ptr, blocks);
            // TODO: Free pages.
        }
    }
}
