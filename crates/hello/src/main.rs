#![no_std]
#![no_main]
extern crate lens_flows;
extern crate lens_inout;

#[no_mangle]
fn main() {
    let stdout = lens_inout::stdout::get().expect("Failed to get standard output");
    stdout.as_ref().send("Hello, world!");
}
