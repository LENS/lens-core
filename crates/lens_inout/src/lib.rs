// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

//! Provides structured standard input and output for LENS programs.

#![no_std]

extern crate alloc;
extern crate lens_files;
extern crate lens_flows;
extern crate lens_messenger;
extern crate lens_syscalls;
extern crate lens_system;
extern crate lens_postcardflows;
extern crate serde;

/// Standard output.
pub mod stdout {
    use alloc::boxed::Box;
    use core::fmt::Display;
    use lens_flows::{byte, object, packet};
    use lens_postcardflows::PostcardSendFilter;

    /// The raw standard output flow.
    pub enum RawOutFlow {
        Byte(Box<dyn byte::Write>),
        Packet(Box<dyn packet::Send>),
    }

    /// Returns a raw flow representing a clone of standard output and boolean indicating if standard output is a terminal, or `None` if no working standard output.
    pub fn raw() -> Option<(RawOutFlow, bool)> {
        let mut junk_buf = [0u8; 16];
        let fd = lens_syscalls::dup(1); // Duplicate standard output handle so we don't close it on drop if it's used multiple times.

        // Determine what kind of kernel object standard output is and create a raw flow accordingly.
        if lens_syscalls::ioctl(
            fd,
            0x5413,
            &mut junk_buf[0] as *mut u8 as *mut core::ffi::c_void,
        ) != -25
        {
            // Standard output is terminal. 0x5413 is an ioctl (`TIOCGWINSZ`) that only works on terminals, and -25 is "inappropriate ioctl for device."
            match lens_files::File::from_descriptor(fd) {
                Ok(f) => {
                    return Some((RawOutFlow::Byte(Box::new(f)), true));
                }
                Err(_) => {
                    //warn!("failed to initialize standard output as terminal");
                    return None;
                }
            }
        }

        if lens_files::File::descriptor_is_file(fd).expect(
            "failed to identify standard output type due to an unexpected error from the kernel",
        ) {
            // Standard output is regular file.
            match lens_files::File::from_descriptor(fd) {
                Ok(f) => {
                    return Some((RawOutFlow::Byte(Box::new(f)), false));
                }
                Err(_) => {
                    //warn!("failed to initialize standard output as file");
                    return None;
                }
            }
        }

        if lens_messenger::Endpoint::descriptor_is_endpoint(fd).expect(
            "failed to identify standard output type due to an unexpected error from the kernel",
        ) {
            // Standard output is messenger endpoint.
            match lens_messenger::Endpoint::from_descriptor(fd) {
                Ok(e) => {
                    return Some((RawOutFlow::Packet(Box::new(e)), false));
                }
                Err(_) => {
                    //warn!("failed to initialize standard output as messenger");
                    return None;
                }
            }
        }
        None
    }

    /// Returns a boxed sendable object flow representing a clone of standard output, or `None` if no working standard output.
    pub fn get<T: serde::Serialize + Display>() -> Option<Box<dyn object::Send<T>>> {
        // Filter raw standard output into an object flow.
        match raw() {
            Some((f, terminal)) => {
                match f {
                    RawOutFlow::Byte(byte) => {
                        if terminal {
                            // Use line packets and `Display`.
                            let line_filter =
                                Box::new(lens_flows::filters::LineSendFilter::new(byte));
                            let display_filter =
                                Box::new(lens_flows::filters::DisplaySendFilter::new(line_filter));

                            Some(display_filter)
                        } else {
                            // Use length-prefix framing and `postcard`.
                            let frame_filter =
                                Box::new(lens_flows::filters::FramedSendFilter::new(byte));
                            let postcard_filter = Box::new(PostcardSendFilter::new(frame_filter));

                            Some(postcard_filter)
                        }
                    }

                    RawOutFlow::Packet(packet) => {
                        let postcard_filter = Box::new(PostcardSendFilter::new(packet));
                        Some(postcard_filter)
                    }
                }
            }
            None => {
                //warn!("failed to initialize standard output");
                None
            }
        }
    }
}

/// Standard input.
pub mod stdin {
    use alloc::boxed::Box;
    use lens_flows::{byte, object, packet};
    use lens_postcardflows::PostcardReceiveFilter;

    /// The raw standard input flow.
    pub enum RawInFlow {
        Byte(Box<dyn byte::Read>),
        Packet(Box<dyn packet::Receive>),
    }

    /// Returns a raw flow representing a clone of standard input, or `None` if no working standard input.
    pub fn raw() -> Option<RawInFlow> {
        let mut junk_buf = [0u8; 16];
        let fd = lens_syscalls::dup(0); // Duplicate standard input handle so we don't close it on drop if it's used multiple times.

        if lens_syscalls::ioctl(
            fd,
            0x5413,
            &mut junk_buf[0] as *mut u8 as *mut core::ffi::c_void,
        ) != -25
            || lens_files::File::descriptor_is_file(fd).expect(
                "failed to identify standard input type due to an unexpected error from the kernel",
            )
        {
            // Standard input is regular file.
            match lens_files::File::from_descriptor(fd) {
                Ok(f) => {
                    return Some(RawInFlow::Byte(Box::new(f)));
                }
                Err(_) => {
                    //warn!("failed to initialize standard input as file");
                    return None;
                }
            }
        }

        if lens_messenger::Endpoint::descriptor_is_endpoint(fd).expect(
            "failed to identify standard input type due to an unexpected error from the kernel",
        ) {
            // Standard input is messenger endpoint.
            match lens_messenger::Endpoint::from_descriptor(fd) {
                Ok(e) => {
                    return Some(RawInFlow::Packet(Box::new(e)));
                }
                Err(_) => {
                    //warn!("failed to initialize standard input as messenger");
                    return None;
                }
            }
        }
        None
    }

    /// Returns a boxed Receivable packet flow that reads lines from a clone of standard input, or `None` if no working standard input.
    pub fn lines() -> Option<Box<dyn packet::Receive>> {
        match raw() {
            Some(f) => {
                match f {
                    RawInFlow::Byte(byte) => {
                        // Use line packets.
                        let line_filter =
                            Box::new(lens_flows::filters::LineReceiveFilter::new(byte));

                        Some(line_filter)
                    }

                    RawInFlow::Packet(packet) => Some(packet),
                }
            }
            None => {
                //warn!("failed to initialize standard input");
                None
            }
        }
    }

    /// Returns a boxed Receivable object flow representing a clone of standard input, or `None` if no working standard input.
    pub fn get<T: for<'de> serde::Deserialize<'de>>() -> Option<Box<dyn object::Receive<T>>> {
        // Filter raw standard input into an object flow.
        match raw() {
            Some(f) => {
                match f {
                    RawInFlow::Byte(byte) => {
                        // Use length-prefix framing and `postcard`.
                        let frame_filter =
                            Box::new(lens_flows::filters::FramedReceiveFilter::new(byte));
                        let postcard_filter = Box::new(PostcardReceiveFilter::new(frame_filter));

                        return Some(postcard_filter);
                    }

                    RawInFlow::Packet(packet) => {
                        let postcard_filter = Box::new(PostcardReceiveFilter::new(packet));

                        return Some(postcard_filter);
                    }
                }
            }
            None => {
                //warn!("failed to initialize standard input");
                None
            }
        }
    }
}
