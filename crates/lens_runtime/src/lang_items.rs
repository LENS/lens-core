// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

use core::panic::PanicInfo;

#[lang = "panic_impl"]
#[no_mangle]
fn rust_begin_panic(_info: &PanicInfo) -> ! {
    lens_syscalls::write(2, b"\nRust panicked.\n" as *const u8, 16);
    lens_syscalls::exit(127);
}

#[lang = "eh_personality"]
#[no_mangle]
fn rust_eh_personality() {
    // Do nothing.
}

#[no_mangle]
pub extern "C" fn _Unwind_Resume() {}

// Should be implemented by compiler_builtins, but can't convince Cargo to build it with the mem feature.
#[no_mangle]
unsafe extern "C" fn bcmp(s1: *const u8, s2: *const u8, n: usize) -> i32 {
    let mut s1 = s1;
    let mut s2 = s2;
    let mut n = n;
    loop {
        if n == 0 {
            return 0;
        }
        if *s1 != *s2 {
            return 1;
        }
        n -= 1;
        s1 = s1.offset(1);
        s2 = s2.offset(1);
    }
}

#[no_mangle]
fn rust_start() {
    extern "C" {
        fn main();
    }
    #[link(name = "rt0", kind = "static")]
    extern "C" {
        fn _start();
    }

    unsafe {
        main();
    }
    lens_syscalls::exit(0);
}
